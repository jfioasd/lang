If you know something, you have its facts within your mind.

eg. 'Do you know sth.?' -> 'Do you have sth. in you?'

(We don't have the word for 'mind', but since the mind is a part of a human, this can also express the meaning.)

---
'mind': the part of you where you put facts inside.
